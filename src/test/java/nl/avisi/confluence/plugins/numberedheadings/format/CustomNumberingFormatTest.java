package nl.avisi.confluence.plugins.numberedheadings.format;

import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsException;

import org.junit.Test;

public class CustomNumberingFormatTest {

    @Test(expected = NumberedHeadingsException.class)
    public void shouldThrowExceptionWhenFormatMethodIsInvoked() {
        new CustomNumberingFormat().format(new int[0]);
    }
}
