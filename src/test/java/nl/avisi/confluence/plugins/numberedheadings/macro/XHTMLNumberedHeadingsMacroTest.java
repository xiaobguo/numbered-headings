package nl.avisi.confluence.plugins.numberedheadings.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.ContentTree;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.google.common.collect.Lists;

import nl.avisi.confluence.plugins.numberedheadings.Heading;
import nl.avisi.confluence.plugins.numberedheadings.format.DecimalNumberingFormat;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameterSupport;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameters;
import nl.avisi.confluence.plugins.numberedheadings.rendering.RenderResult;

import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class XHTMLNumberedHeadingsMacroTest {

    private static final String TL_RENDER_RESULT_FIELD_NAME = "RENDER_RESULT_TL";

    @Test
    public void shouldNotRenderMacroWhenIncludedInAnotherPage() throws Exception {
        ConversionContext conversionContext = createConversionContext(true, true);

        String macroBody = "<h1>Heading 1</h1>";

        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(null);
        String result = macro.execute(null, macroBody, conversionContext);
        assertEquals(macroBody, result);
    }

    @Test
    public void shouldRenderMacroWhenNotIncludedInAnotherPage() throws Exception {
        ConversionContext conversionContext = createConversionContext(false, false);

        String macroBody = "<h1>Heading 1</h1>";
        String expectedResult = "<h1><span class=\"nh-number\">1. </span>Heading 1</h1>";

        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(createNumberedHeadingsParameterSupport());
        String result = macro.execute(null, macroBody, conversionContext);
        assertEquals(expectedResult, result);
    }

    @Test
    public void shouldUseEndingNumbersFromPreviousExecute() throws Exception {
        ConversionContext conversionContext = createConversionContext(false, true, new Page(), new Page());

        String macroBody = "<h1>Heading 1</h1>";

        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(createNumberedHeadingsParameterSupport());
        String result = macro.execute(null, macroBody, conversionContext);
        assertEquals("<h1><span class=\"nh-number\">1. </span>Heading 1</h1>", result);

        result = macro.execute(null, macroBody, conversionContext);
        assertEquals("<h1><span class=\"nh-number\">2. </span>Heading 1</h1>", result);
    }

    @Test
    public void shouldCleanUpThreadLocalAfterLastPage() throws Exception {
        Page lastPage = new Page();
        ConversionContext conversionContext = createConversionContext(false, true, new Page(), lastPage);
        when(conversionContext.getEntity()).thenReturn(lastPage);

        String macroBody = "<h1>Heading 1</h1>";

        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(createNumberedHeadingsParameterSupport());
        macro.execute(null, macroBody, conversionContext);
        macro.execute(null, macroBody, conversionContext);

        RenderResult renderResult = getRenderResultFromMacro(macro);
        assertNull(renderResult);
    }

    @Test
    // Issue #23, #24, #25, #26
    public void shouldNotStoreRenderingResultInThreadLocalWhenThereAreZeroPages() throws Exception {
        Page lastPage = new Page();
        ConversionContext conversionContext = createConversionContext(false, true);
        when(conversionContext.getEntity()).thenReturn(lastPage);

        String macroBody = "<h1>Heading 1</h1>";

        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(createNumberedHeadingsParameterSupport());
        macro.execute(null, macroBody, conversionContext);
        macro.execute(null, macroBody, conversionContext);

        RenderResult renderResult = getRenderResultFromMacro(macro);
        assertNull(renderResult);
    }

    @Test
    public void shouldNotUseEndingNumbersWhenThereIsOnlyOnePage() throws Exception {
        ConversionContext conversionContext = createConversionContext(false, true, new Page());

        String macroBody = "<h1>Heading 1</h1>";

        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(createNumberedHeadingsParameterSupport());
        String result = macro.execute(null, macroBody, conversionContext);
        assertEquals("<h1><span class=\"nh-number\">1. </span>Heading 1</h1>", result);

        result = macro.execute(null, macroBody, conversionContext);
        assertEquals("<h1><span class=\"nh-number\">1. </span>Heading 1</h1>", result);
    }

    @Test
    public void macroBodyContentShouldBeRichText() {
        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(null);
        assertEquals(Macro.BodyType.RICH_TEXT, macro.getBodyType());
    }

    @Test
    public void macroOutputTypeIsBlock() {
        XHTMLNumberedHeadingsMacro macro = new XHTMLNumberedHeadingsMacro(null);
        assertEquals(Macro.OutputType.BLOCK, macro.getOutputType());
    }

    private ConversionContext createConversionContext(boolean includedInOtherPage, boolean contentTreeExists, Page... pages) {
        PageContext mockedPageContext = mock(PageContext.class);
        DefaultConversionContext mockedConversionContext = mock(DefaultConversionContext.class);
        when(mockedConversionContext.getRenderContext()).thenReturn(mockedPageContext);

        if (!includedInOtherPage) {
            when(mockedPageContext.getOriginalContext()).thenReturn(mockedPageContext);
        }

        if (contentTreeExists) {
            ContentTree mockedContentTree = mock(ContentTree.class);
            when(mockedConversionContext.getContentTree()).thenReturn(mockedContentTree);

            when(mockedContentTree.getPages()).thenReturn(Lists.newArrayList(pages));
        }

        return mockedConversionContext;
    }

    private NumberedHeadingsParameterSupport createNumberedHeadingsParameterSupport() throws Exception {
        NumberedHeadingsParameters numberedHeadingsParameters = new NumberedHeadingsParameters();
        numberedHeadingsParameters.setStartNumberingAt(Heading.H1);
        numberedHeadingsParameters.setNumberFormat(new DecimalNumberingFormat());

        NumberedHeadingsParameterSupport mockedParameterSupport = mock(NumberedHeadingsParameterSupport.class);
        when(mockedParameterSupport.parseParameters(anyMap())).thenReturn(numberedHeadingsParameters);

        return mockedParameterSupport;
    }

    private RenderResult getRenderResultFromMacro(XHTMLNumberedHeadingsMacro macro) {
        ThreadLocal<RenderResult> tl = (ThreadLocal<RenderResult>) Whitebox.getInternalState(macro, TL_RENDER_RESULT_FIELD_NAME);

        return tl.get();
    }
}
