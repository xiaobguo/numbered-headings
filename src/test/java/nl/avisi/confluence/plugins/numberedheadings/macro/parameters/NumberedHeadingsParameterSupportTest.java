package nl.avisi.confluence.plugins.numberedheadings.macro.parameters;

import com.atlassian.renderer.v2.macro.basic.validator.MacroParameterValidationException;

import nl.avisi.confluence.plugins.numberedheadings.NumberFormatterManager;
import nl.avisi.confluence.plugins.numberedheadings.format.*;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NumberedHeadingsParameterSupportTest {

    private NumberedHeadingsParameterSupport numberedHeadingsParameterSupport;

    @Before
    public void beforeTest() throws Exception {
        NumberFormatterManager numberFormatterManager = mock(NumberFormatterManager.class);
        when(numberFormatterManager.fromString("custom")).thenReturn(new CustomNumberingFormat());
        when(numberFormatterManager.fromString("decimal")).thenReturn(new DecimalNumberingFormat());
        when(numberFormatterManager.fromString("upper-roman")).thenReturn(new UpperRomanNumberingFormat());
        when(numberFormatterManager.fromString("FOO")).thenReturn(new DecimalNumberingFormat());

        numberedHeadingsParameterSupport = new NumberedHeadingsParameterSupport(numberFormatterManager);
    }

    @Test
    public void testValidCustomFormat() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        int[] breadcrumbs = {2};

        parameters.put("number-format", "custom");
        parameters.put("h1", "Chapter [h1.decimal]");

        NumberedHeadingsParameters pars = numberedHeadingsParameterSupport.parseParameters(parameters);

        List<List<NumberingFormat>> numberFormatters = pars.getNumberFormatters();
        List<NumberingFormat> Heading1Formatters = numberFormatters.get(0);

        assertEquals(2, Heading1Formatters.size());
        assertEquals(LiteralFormat.class, Heading1Formatters.get(0).getClass());
        assertEquals(DecimalNumberingFormat.class, Heading1Formatters.get(1).getClass());

        assertEquals("Chapter ", Heading1Formatters.get(0).format(null));
        assertEquals("2", Heading1Formatters.get(1).format(breadcrumbs));
    }

    @Test
    public void testInvalidCustomFormat1() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        List<Integer> breadcrumbs = new ArrayList<Integer>();
        breadcrumbs.add(2);

        parameters.put("number-format", "custom");
        parameters.put("h1", "Chapter [a1.decimal]");

        NumberedHeadingsParameters pars = numberedHeadingsParameterSupport.parseParameters(parameters);

        List<List<NumberingFormat>> numberFormatters = pars.getNumberFormatters();
        List<NumberingFormat> Heading1Formatters = numberFormatters.get(0);

        assertEquals(1, Heading1Formatters.size());
        assertEquals(LiteralFormat.class, Heading1Formatters.get(0).getClass());
        assertEquals("Chapter [a1.decimal]", Heading1Formatters.get(0).format(null));
    }

    @Test
    public void testInvalidCustomFormat2() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        int[] breadcrumbs = {2};

        parameters.put("number-format", "custom");
        parameters.put("h1", "Chapter [h1.FOO]");

        NumberedHeadingsParameters pars = numberedHeadingsParameterSupport.parseParameters(parameters);

        List<List<NumberingFormat>> numberFormatters = pars.getNumberFormatters();
        List<NumberingFormat> Heading1Formatters = numberFormatters.get(0);

        assertEquals(2, Heading1Formatters.size());
        assertEquals(LiteralFormat.class, Heading1Formatters.get(0).getClass());
        assertEquals(DecimalNumberingFormat.class, Heading1Formatters.get(1).getClass());

        assertEquals("Chapter ", Heading1Formatters.get(0).format(null));
        assertEquals("2", Heading1Formatters.get(1).format(breadcrumbs));
    }

    @Test
    public void shouldReturnStartNumberingWithValueWhenBiggerThanZero() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "upper-roman");
        parameters.put("start-numbering-with", "2");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);
        assertArrayEquals(new int[]{1, 0, 0, 0, 0, 0}, numberedHeadingsParameters.getStartingNumbers());
    }

    @Test
    public void shouldReturnStartNumberingWithValueWhenZeroAndFormatterSupportsIt() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("start-numbering-with", "0");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);
        assertArrayEquals(new int[]{-1, 0, 0, 0, 0, 0}, numberedHeadingsParameters.getStartingNumbers());
    }

    @Test
    public void shouldThrowAnExceptionWhenFormatterDoesNotSupportZeroBasedFormatting() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "upper-roman");
        parameters.put("start-numbering-with", "0");

        try {
            numberedHeadingsParameterSupport.parseParameters(parameters);
            fail("Expected MacroParameterValidationException");
        } catch (MacroParameterValidationException e) {
            assertEquals("Selected number format does not support numbering starting at 0", e.getMessage());
        }
    }

    @Test(expected = MacroParameterValidationException.class)
    public void shouldThrowMacroParameterValidationExceptionWhenStartingWithIsNotValidNumber() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("start-numbering-with", "FOOBAR");

        numberedHeadingsParameterSupport.parseParameters(parameters);
    }

    @Test(expected = MacroParameterValidationException.class)
    public void shouldThrowMacroParameterValidationExceptionWhenStartingWithIsTooBig() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("start-numbering-with", "999999999999");

        numberedHeadingsParameterSupport.parseParameters(parameters);
    }

    @Test
    public void shouldReturnCorrectlyParseSkipHeadingsParameter() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("skip-headings", "h1,h4");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);

        assertArrayEquals(new int[]{1, 4}, numberedHeadingsParameters.getSkipHeadings());
    }

    @Test
    public void shouldIgnoreCasingWhenParsingSkipHeadingsParameter() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("skip-headings", "H1,h4");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);

        assertArrayEquals(new int[]{1, 4}, numberedHeadingsParameters.getSkipHeadings());
    }

    @Test
    public void shouldIgnoreSpacesWhenParsingSkipHeadingsParameter() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("skip-headings", "h1 ,  h4");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);

        assertArrayEquals(new int[]{1, 4}, numberedHeadingsParameters.getSkipHeadings());
    }

    @Test
    public void shouldIgnoreInvalidHeadingsWhenParsingSkipheadingsParameter() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");
        parameters.put("skip-headings", "h1,a4");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);

        assertArrayEquals(new int[]{1}, numberedHeadingsParameters.getSkipHeadings());
    }

    @Test
    public void shouldReturnEmptyArrayWhenSkipHeadingsParameterDoesNotExist() throws Exception {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("number-format", "decimal");

        NumberedHeadingsParameters numberedHeadingsParameters = numberedHeadingsParameterSupport.parseParameters(parameters);

        assertTrue(ArrayUtils.isEmpty(numberedHeadingsParameters.getSkipHeadings()));
    }
}
