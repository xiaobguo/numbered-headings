package nl.avisi.confluence.plugins.numberedheadings.macro;

import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.basic.validator.MacroParameterValidationException;

import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsException;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameterSupport;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NumberedHeadingsMacroTest {

    @Test
    public void macroIsNotInline() {
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(null);
        assertFalse(numberedHeadingsMacro.isInline());
    }

    @Test
    public void macroHasBody() {
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(null);
        assertTrue(numberedHeadingsMacro.hasBody());
    }

    @Test
    public void macroShouldNotSuppressSurroundingTagDuringWysiwygRendering() {
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(null);
        assertFalse(numberedHeadingsMacro.suppressSurroundingTagDuringWysiwygRendering());
    }

    @Test
    public void macroShouldSuppressMacroRenderingDuringWysiwyg() {
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(null);
        assertTrue(numberedHeadingsMacro.suppressMacroRenderingDuringWysiwyg());
    }

    @Test
    public void getBodyRenderMode() {
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(null);
        assertEquals(RenderMode.ALL, numberedHeadingsMacro.getBodyRenderMode());
    }

    @Test
    public void testExecuteWithoutInclude() throws MacroException {
        NumberedHeadingsParameterSupport numberedHeadingsParameterSupport = new NumberedHeadingsParameterSupport(null);
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(numberedHeadingsParameterSupport);

        String macroBody = "<h1>Heading 1</h1>\n\n<h1>Heading 2</h1>";

        String result = numberedHeadingsMacro.execute(Collections.emptyMap(), macroBody, new PageContext());

        assertEquals("<h1><span class=\"nh-number\">1. </span>Heading 1</h1>\n\n<h1><span class=\"nh-number\">2. </span>Heading 2</h1>", result);
    }

    @Test
    public void shouldCatchMacroExceptionAndReThrow() throws MacroException {
        MacroParameterValidationException exception = new MacroParameterValidationException("");

        NumberedHeadingsParameterSupport mockedNumberedHeadingsParameterSupport = mock(NumberedHeadingsParameterSupport.class);
        when(mockedNumberedHeadingsParameterSupport.parseParameters(anyMap())).thenThrow(exception);

        try {
            new NumberedHeadingsMacro(mockedNumberedHeadingsParameterSupport).execute(null, null, new PageContext());
            fail("Expected MacroParameterValidationException");
        } catch (MacroParameterValidationException e) {
            assertEquals(exception, e);
        }
    }

    @Test
    public void shouldCatchNumberedHeadingsExceptionAndWrapInMacroException() throws MacroException {
        NumberedHeadingsException exception = new NumberedHeadingsException("");

        NumberedHeadingsParameterSupport mockedNumberedHeadingsParameterSupport = mock(NumberedHeadingsParameterSupport.class);
        when(mockedNumberedHeadingsParameterSupport.parseParameters(anyMap())).thenThrow(exception);

        try {
            new NumberedHeadingsMacro(mockedNumberedHeadingsParameterSupport).execute(null, null, new PageContext());
            fail("Expected NumberedHeadingsException");
        } catch (MacroException e) {
            assertEquals(exception, e.getCause());
        }
    }

    @Test(expected = MacroException.class)
    public void shouldCatchAllExceptionsAndThrowMacroException() throws MacroException {
        new NumberedHeadingsMacro(null).execute(null, null, null);
    }

    @Test
    public void shouldReturnMacroBodyIfMacroPageIsIncluded() throws MacroException {
        NumberedHeadingsMacro numberedHeadingsMacro = new NumberedHeadingsMacro(null);
        String macroBody = "Lorum Lipsum";
        PageContext pageContext = mock(PageContext.class);
        when(pageContext.getOriginalContext()).thenReturn(new PageContext());

        String macroRenderingResult = numberedHeadingsMacro.execute(Collections.emptyMap(), macroBody, pageContext);

        assertEquals(macroBody, macroRenderingResult);
    }
}
