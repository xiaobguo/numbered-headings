package nl.avisi.confluence.plugins.numberedheadings;

import nl.avisi.confluence.plugins.numberedheadings.format.DecimalNumberingFormat;
import nl.avisi.confluence.plugins.numberedheadings.format.NumberingFormat;

import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import static org.junit.Assert.assertTrue;

public class NumberFormatterDescriptorTest {

    @Test
    public void shouldReturnInstanceOfConfiguredModuleClass() throws Exception {
        NumberFormatterDescriptor numberFormatterDescriptor = new NumberFormatterDescriptor();
        Whitebox.setInternalState(numberFormatterDescriptor, "moduleClass", DecimalNumberingFormat.class);

        NumberingFormat numberingFormat = numberFormatterDescriptor.getModule();

        assertTrue(numberingFormat instanceof DecimalNumberingFormat);
    }
}
