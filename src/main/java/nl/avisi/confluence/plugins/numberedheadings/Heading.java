package nl.avisi.confluence.plugins.numberedheadings;

import java.util.Locale;

public enum Heading {

    H1(1),
    H2(2),
    H3(3),
    H4(4),
    H5(5),
    H6(6);

    public static final int NUMBER_OF_HEADINGS = 6;

    private final int level;

    private Heading(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public static Heading getEnum(String name) {
        return valueOf(name.toUpperCase(Locale.ENGLISH));
    }
}
