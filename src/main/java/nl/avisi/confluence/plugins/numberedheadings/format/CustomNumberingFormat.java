package nl.avisi.confluence.plugins.numberedheadings.format;

import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsException;

public class CustomNumberingFormat extends AbstractFormat {

    @Override
    public String format(int[] breadcrumbs, Integer level) {
        throw new NumberedHeadingsException("This is a placeholder class, it should never be invoked!");
    }
}
