package nl.avisi.confluence.plugins.numberedheadings.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.ContentTree;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;

import nl.avisi.confluence.plugins.numberedheadings.Utils;
import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsMacroException;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameterSupport;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameters;
import nl.avisi.confluence.plugins.numberedheadings.rendering.HeadingRenderer;
import nl.avisi.confluence.plugins.numberedheadings.rendering.RenderResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Render outlined numbers for all levels of the headings.
 */
public class XHTMLNumberedHeadingsMacro implements Macro {

    private static final Logger LOGGER = LoggerFactory.getLogger(XHTMLNumberedHeadingsMacro.class);

    private static final ThreadLocal<RenderResult> RENDER_RESULT_TL = new ThreadLocal<RenderResult>();

    private NumberedHeadingsParameterSupport numberedHeadingsParameterSupport;

    public XHTMLNumberedHeadingsMacro(NumberedHeadingsParameterSupport numberedHeadingsParameterSupport) {
        this.numberedHeadingsParameterSupport = numberedHeadingsParameterSupport;
    }

    @Override
    public String execute(Map<String, String> parameters, String macroBody, ConversionContext conversionContext) throws MacroExecutionException {
        try {
            // Do not render if this macro is in a included page
            if (isPageIncluded(conversionContext)) {
                return macroBody;
            } else {
                return render(parameters, macroBody, conversionContext);
            }
        } catch (MacroException e) {
            return Utils.renderErrorBlock(ConfluenceActionSupport.getTextStatic("nl.avisi.confluence.plugins.numberedheadings.unable-to-render"), e.getMessage());
        } catch (NumberedHeadingsMacroException e) {
            return Utils.renderErrorBlock(ConfluenceActionSupport.getTextStatic("nl.avisi.confluence.plugins.numberedheadings.unable-to-render"), e.getMessage());
        } catch (Exception e) {
            LOGGER.warn("The Numbered Headings plugin threw an unexpected exception...", e);

            return Utils.renderErrorBlock(ConfluenceActionSupport.getTextStatic("nl.avisi.confluence.plugins.numberedheadings.unable-to-render"), ConfluenceActionSupport.getTextStatic("nl.avisi.confluence.plugins.numberedheadings.unexpected-error"));
        }
    }

    /**
     * Parses the given parameters and renders the macro body with the parsed parameters
     *
     * @param parameters the parameters of the macro
     * @param macroBody  the body of the macro
     * @return a numbered headings rendered macro body
     */
    private String render(Map<String, String> parameters, String macroBody, ConversionContext conversionContext) throws MacroException {
        NumberedHeadingsParameters parsedParameters = numberedHeadingsParameterSupport.parseParameters(parameters);

        ContentTree contentTree = conversionContext.getContentTree();

        if (contentTree != null) {
            List<Page> pages = contentTree.getPages();
            if (pages.size() > 1) { // If there is only one page, it has no use to store rendering result (includes are no in this list)
                RenderResult previousRendererResult = RENDER_RESULT_TL.get();
                if (previousRendererResult != null) {
                    parsedParameters.setStartingNumbers(previousRendererResult.getEndingNumbers());
                }
            }
        }

        RenderResult rendererResult = new HeadingRenderer(parsedParameters).render(macroBody);

        if (contentTree != null) {
            List<Page> pages = contentTree.getPages();

            // Issue #23, #24, #25, #26...
            // If NH is in a blogpost or when creating a global template (and maybe some other cases), there are no pages... which will cause an index out of bounds
            if (pages.size() == 0 || conversionContext.getEntity() == pages.get(pages.size() - 1)) {    // Last page, so we have to do some cleanup
                RENDER_RESULT_TL.set(null);
            } else {
                RENDER_RESULT_TL.set(rendererResult);
            }
        }

        return rendererResult.getResult();
    }

    /**
     * Checks if the current page (where this macro lives) is included in
     * another page
     *
     * @param conversionContext the conversionContext of the page to check
     * @return true if this page is included, otherwise false
     */
    private boolean isPageIncluded(ConversionContext conversionContext) {
        RenderContext renderContext = ((DefaultConversionContext) conversionContext).getRenderContext();
        return ((PageContext) renderContext).getOriginalContext() != renderContext;
    }

    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
