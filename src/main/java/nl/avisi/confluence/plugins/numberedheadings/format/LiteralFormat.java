package nl.avisi.confluence.plugins.numberedheadings.format;

public class LiteralFormat extends AbstractFormat {

    private final String literal;

    public LiteralFormat(String literal) {
        this.literal = literal;
    }

    @Override
    public String format(int[] breadCrumbs, Integer level) {
        return literal;
    }
}
