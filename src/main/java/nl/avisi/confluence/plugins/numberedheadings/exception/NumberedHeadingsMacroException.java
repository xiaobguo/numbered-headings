package nl.avisi.confluence.plugins.numberedheadings.exception;

public class NumberedHeadingsMacroException extends NumberedHeadingsException {

    public NumberedHeadingsMacroException(String message) {
        super(message);
    }
}
