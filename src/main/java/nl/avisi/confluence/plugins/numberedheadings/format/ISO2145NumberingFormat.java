package nl.avisi.confluence.plugins.numberedheadings.format;

public class ISO2145NumberingFormat extends DecimalNumberingFormat {

    @Override
    public boolean shouldRenderSeparator(boolean last) {
        return !last;
    }
}
