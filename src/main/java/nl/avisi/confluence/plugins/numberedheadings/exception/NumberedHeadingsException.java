package nl.avisi.confluence.plugins.numberedheadings.exception;

/**
 * Use this class only for technical exceptions that should not be shown to the user (the user will get a generic error message).
 */
public class NumberedHeadingsException extends RuntimeException {

    public NumberedHeadingsException(String message) {
        super(message);
    }

    public NumberedHeadingsException(String message, Throwable cause) {
        super(message, cause);
    }
}
