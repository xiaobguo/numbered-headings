package nl.avisi.confluence.plugins.numberedheadings;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

@Path("/formatter")
public class NumberedHeadingsRestResource {

    private final NumberFormatterManager numberFormatterManager;

    public NumberedHeadingsRestResource(NumberFormatterManager numberFormatterManager) {
        this.numberFormatterManager = numberFormatterManager;
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getEnabledFormatters() {
        List<String> numberFormatterDescriptorNames = numberFormatterManager.getNumberFormatterDescriptorNames();

        return Response.ok(numberFormatterDescriptorNames).build();
    }
}
