package nl.avisi.confluence.plugins.numberedheadings.macro;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsMacroException;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameterSupport;
import nl.avisi.confluence.plugins.numberedheadings.macro.parameters.NumberedHeadingsParameters;
import nl.avisi.confluence.plugins.numberedheadings.rendering.HeadingRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Render outlined numbers for all levels of the headings.
 */
public class NumberedHeadingsMacro extends BaseMacro {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberedHeadingsMacro.class);

    private final NumberedHeadingsParameterSupport numberedHeadingsParameterSupport;

    public NumberedHeadingsMacro(NumberedHeadingsParameterSupport numberedHeadingsParameterSupport) {
        this.numberedHeadingsParameterSupport = numberedHeadingsParameterSupport;
    }

    @SuppressWarnings("PMD.AvoidRethrowingException")
    public final String execute(Map parameters, String macroBody, RenderContext renderContext) throws MacroException {
        try {
            // Do not render if this macro is in a included page
            if (isPageIncluded(renderContext)) {
                return macroBody;
            } else {
                return render(parameters, macroBody);
            }
        } catch (MacroException e) {
            throw e;
        } catch (NumberedHeadingsMacroException e) {
            throw new MacroException(e);
        } catch (Exception e) {
            LOGGER.warn("The Numbered Headings plugin threw an unexpected exception...", e);
            throw new MacroException(ConfluenceActionSupport.getTextStatic("nl.avisi.confluence.plugins.numberedheadings.unexpected-error"), e);
        }
    }

    /**
     * Parses the given parameters and renders the macro body with the parsed parameters
     *
     * @param parameters the parameters of the macro
     * @param macroBody  the body of the macro
     * @return a numbered headings rendered macro body
     */
    private String render(Map parameters, String macroBody) throws MacroException {
        NumberedHeadingsParameters parsedParameters = numberedHeadingsParameterSupport.parseParameters(parameters);
        return new HeadingRenderer(parsedParameters).render(macroBody).getResult();
    }

    /**
     * Checks if the current page (where this macro lives) is included in another page
     *
     * @param renderContext the rendercontext of the page to check
     * @return true if this page is included, otherwise false
     */
    private boolean isPageIncluded(RenderContext renderContext) {
        return ((PageContext) renderContext).getOriginalContext() != renderContext;
    }

    public final boolean isInline() {
        return false;
    }

    public final boolean hasBody() {
        return true;
    }

    // suppressSurroundingTag | suppressMacroRendering | what happens
    // true | true | switching back and forth: {tags} dissapear in wiki markup
    // false | true | seems to work, shows {tags} in wysiwyg
    // false | false | need to write {tags} in wysiwyg
    // true | false | works, does not show {tags} in wysiwyg

    public final boolean suppressSurroundingTagDuringWysiwygRendering() {
        return false;
    }

    public final boolean suppressMacroRenderingDuringWysiwyg() {
        return true;
    }

    public final RenderMode getBodyRenderMode() {
        return RenderMode.ALL;
    }
}
