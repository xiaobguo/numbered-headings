package nl.avisi.confluence.plugins.numberedheadings.format;

public class LowerLatinNumberingFormat extends UpperLatinNumberingFormat {

    @Override
    public String format(int[] breadCrumbs, Integer level) {
        return super.format(breadCrumbs, level).toLowerCase();
    }
}
