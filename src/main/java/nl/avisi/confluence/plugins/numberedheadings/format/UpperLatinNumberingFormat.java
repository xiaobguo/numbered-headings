package nl.avisi.confluence.plugins.numberedheadings.format;

public class UpperLatinNumberingFormat extends CharacterRangeFormat {

    private static final String[] UPPER_LATIN_CHARACTERS = {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

    public UpperLatinNumberingFormat() {
        super(UPPER_LATIN_CHARACTERS);
    }
}
