package nl.avisi.confluence.plugins.numberedheadings.format;

public interface NumberingFormat {

    String format(int[] breadcrumbs);

    String format(int[] breadcrumbs, Integer level);

    boolean shouldRenderSeparator(boolean last);

    boolean supportsZeroBasedNumbering();

    void setLevel(Integer level);
}
