package nl.avisi.confluence.plugins.numberedheadings.macro.parameters;

import nl.avisi.confluence.plugins.numberedheadings.Heading;
import nl.avisi.confluence.plugins.numberedheadings.format.NumberingFormat;

import java.util.Arrays;
import java.util.List;

import static nl.avisi.confluence.plugins.numberedheadings.Heading.NUMBER_OF_HEADINGS;

public class NumberedHeadingsParameters {

    private int[] startingNumbers = new int[NUMBER_OF_HEADINGS];
    private int[] skipHeadings = new int[NUMBER_OF_HEADINGS];
    private Heading startNumberingAt;
    private NumberingFormat numberingFormat;
    private List<List<NumberingFormat>> numberFormatters;

    public final Heading getStartNumberingAt() {
        return startNumberingAt;
    }

    public final void setStartNumberingAt(Heading startNumberingAt) {
        this.startNumberingAt = startNumberingAt;
    }

    public final int[] getStartingNumbers() {
        return Arrays.copyOf(startingNumbers, startingNumbers.length);
    }

    public int[] getSkipHeadings() {
        return Arrays.copyOf(skipHeadings, skipHeadings.length);
    }

    public void setSkipHeadings(int[] skipHeadings) {
        this.skipHeadings = skipHeadings;
    }

    public final NumberingFormat getNumberFormat() {
        return numberingFormat;
    }

    public final void setNumberFormat(NumberingFormat numberingFormat) {
        this.numberingFormat = numberingFormat;
    }

    public final List<List<NumberingFormat>> getNumberFormatters() {
        return numberFormatters;
    }

    public final void setNumberFormatters(List<List<NumberingFormat>> numberFormatters) {
        this.numberFormatters = numberFormatters;
    }

    public void setStartingNumbers(int[] startingNumbers) {
        this.startingNumbers = startingNumbers;
    }

    public void setStartingNumber(int headingLevel, int startingNumber) {
        this.startingNumbers[headingLevel - 1] = startingNumber - 1;
    }
}
