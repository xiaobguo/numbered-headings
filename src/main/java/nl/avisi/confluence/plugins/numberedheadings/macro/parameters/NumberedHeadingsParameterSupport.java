package nl.avisi.confluence.plugins.numberedheadings.macro.parameters;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.gzipfilter.org.apache.commons.lang.ArrayUtils;
import com.atlassian.renderer.v2.macro.basic.validator.MacroParameterValidationException;

import nl.avisi.confluence.plugins.numberedheadings.Heading;
import nl.avisi.confluence.plugins.numberedheadings.NumberFormatterManager;
import nl.avisi.confluence.plugins.numberedheadings.Utils;
import nl.avisi.confluence.plugins.numberedheadings.format.CustomNumberingFormat;
import nl.avisi.confluence.plugins.numberedheadings.format.DecimalNumberingFormat;
import nl.avisi.confluence.plugins.numberedheadings.format.LiteralFormat;
import nl.avisi.confluence.plugins.numberedheadings.format.NumberingFormat;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static nl.avisi.confluence.plugins.numberedheadings.Heading.NUMBER_OF_HEADINGS;

/**
 * Responsible for checking the parameters passed to the Macro.
 */
public class NumberedHeadingsParameterSupport {

    private final NumberFormatterManager numberFormatterManager;

    private static final String SKIP_HEADINGS_REGEX = "(h|H)[1-6]";
    private static final String CUSTOM_HEADING_REGEX = "\\[h[1-6]\\.[a-zA-Z0-9\\-]+\\]";
    private static final String CUSTOM_FORMATTER_NAME = "custom";

    /* Parameter keys */
    private static final String NUMBER_FORMAT_KEY = "number-format";
    private static final String START_NUMBERING_AT_KEY = "start-numbering-at";
    private static final String START_NUMBERING_WITH_KEY = "start-numbering-with";
    private static final String SKIP_HEADINGS_KEY = "skip-headings";

    /* Default parameter values */
    private static final Heading DEFAULT_START_NUMBERING_AT = Heading.H1;
    private static final int DEFAULT_START_NUMBERING_WITH = 1;
    private static final NumberingFormat DEFAULT_NUMBERING_FORMAT = new DecimalNumberingFormat();

    public NumberedHeadingsParameterSupport(NumberFormatterManager numberFormatterManager) {
        this.numberFormatterManager = numberFormatterManager;
    }

    public NumberedHeadingsParameters parseParameters(Map<String, String> parameters) throws MacroParameterValidationException {
        NumberedHeadingsParameters parsedParameters = new NumberedHeadingsParameters();

        NumberingFormat numberingFormatParameter = parseNumberFormatParameter(parameters);
        Heading startNumberingAt = parseStartNumberingAtParameter(parameters);

        parsedParameters.setStartingNumber(startNumberingAt.getLevel(), parseStartNumberingWithParameter(parameters, numberingFormatParameter));
        parsedParameters.setStartNumberingAt(startNumberingAt);
        parsedParameters.setSkipHeadings(parseSkipHeadings(parameters));
        parsedParameters.setNumberFormat(numberingFormatParameter);
        parsedParameters.setNumberFormatters(parseNumberFormatters(parameters, numberingFormatParameter, startNumberingAt));

        return parsedParameters;
    }

    @SuppressWarnings("PMD.PreserveStackTrace")
    private int parseStartNumberingWithParameter(Map<String, String> parameters, NumberingFormat numberingFormat) throws MacroParameterValidationException {
        String parameterValue = parameters.get(START_NUMBERING_WITH_KEY);

        if (!StringUtils.isEmpty(parameterValue)) {
            int startNumberingWith;
            try {
                startNumberingWith = Integer.parseInt(parameterValue);
            } catch (NumberFormatException e) {
                throw new MacroParameterValidationException("'Start numbering with' value is not a valid number");
            }

            if (startNumberingWith > 0) {
                return startNumberingWith;
            } else if (numberingFormat.supportsZeroBasedNumbering() && startNumberingWith == 0) {
                return startNumberingWith;
            } else {
                throw new MacroParameterValidationException("Selected number format does not support numbering starting at " + startNumberingWith);
            }
        }

        return DEFAULT_START_NUMBERING_WITH;
    }

    private Heading parseStartNumberingAtParameter(Map<String, String> parameters) {
        String parameterValue = parameters.get(START_NUMBERING_AT_KEY);

        if (!StringUtils.isEmpty(parameterValue)) {
            return Heading.getEnum(parameterValue);
        }

        return DEFAULT_START_NUMBERING_AT;
    }

    private int[] parseSkipHeadings(Map<String, String> parameters) {
        String parameterValue = parameters.get(SKIP_HEADINGS_KEY);

        if (!StringUtils.isEmpty(parameterValue)) {
            String[] headings = parameterValue.split(",");
            int[] skipHeadings = new int[0];

            for (String heading : headings) {
                String trimmedHeading = heading.trim();

                if (trimmedHeading.matches(SKIP_HEADINGS_REGEX)) {
                    int headingLevel = Integer.parseInt(Character.toString(trimmedHeading.charAt(1)));

                    skipHeadings = ArrayUtils.add(skipHeadings, headingLevel);
                }
            }

            return skipHeadings;
        }

        return new int[0];
    }

    private NumberingFormat parseNumberFormatParameter(Map<String, String> parameters) {
        String parameterValue = parameters.get(NUMBER_FORMAT_KEY);

        if (!StringUtils.isEmpty(parameterValue)) {
            return numberFormatterManager.fromString(parameterValue);
        }

        return DEFAULT_NUMBERING_FORMAT;
    }

    private List<List<NumberingFormat>> parseNumberFormatters(Map<String, String> parameters, NumberingFormat numberingFormat, Heading startNumberingAt) {
        List<List<NumberingFormat>> numberFormatters = new ArrayList<List<NumberingFormat>>(NUMBER_OF_HEADINGS);

        if (numberingFormat instanceof CustomNumberingFormat) {
            for (Heading heading : Heading.values()) {
                String lowerCaseHeading = heading.toString().toLowerCase();

                if (parameters.containsKey(lowerCaseHeading) && heading.getLevel() >= startNumberingAt.getLevel()) {
                    String customHeadingValue = parameters.get(lowerCaseHeading);
                    numberFormatters.add(parseCustomHeadingValue(heading.getLevel(), customHeadingValue, startNumberingAt));
                } else {
                    numberFormatters.add(new ArrayList<NumberingFormat>());
                }
            }
        }

        return numberFormatters;
    }

    private List<NumberingFormat> parseCustomHeadingValue(Integer level, String customHeadingValue, Heading startNumberingAt) {
        List<NumberingFormat> customFormatters = new ArrayList<NumberingFormat>();
        String[] formatters = Utils.splitAndPreserveToken(customHeadingValue, CUSTOM_HEADING_REGEX);

        for (String formatter : formatters) {
            NumberingFormat numberingFormat = parseFormatter(level, formatter, startNumberingAt);

            if (numberingFormat != null) {
                customFormatters.add(numberingFormat);
            }
        }

        return customFormatters;
    }

    private NumberingFormat parseFormatter(Integer level, String headingFormatter, Heading startNumberingAt) {
        NumberingFormat numberingFormat = null;

        if (headingFormatter.matches(CUSTOM_HEADING_REGEX)) {
            // Remove first [ and last ]
            String headingFormatterWithoutBrackets = headingFormatter.substring(1, headingFormatter.length() - 1);

            // Separate heading and formatter; "h1.decimal" gives ["h1", "decimal"]
            String[] formatterSettings = headingFormatterWithoutBrackets.split("\\.");
            Integer formatterLevel = Heading.getEnum(formatterSettings[0]).getLevel();

            if (startNumberingAt.getLevel() <= formatterLevel && formatterLevel <= level) {
                String formatter = formatterSettings[1];

                if (!CUSTOM_FORMATTER_NAME.equals(formatter)) {
                    numberingFormat = createNumberingFormat(formatter, formatterLevel);
                }
            }
        } else {
            numberingFormat = createLiteralFormat(headingFormatter);
        }

        return numberingFormat;
    }

    private NumberingFormat createNumberingFormat(String formatter, Integer level) {
        NumberingFormat numberingFormat = numberFormatterManager.fromString(formatter);
        numberingFormat.setLevel(level);

        return numberingFormat;
    }

    private LiteralFormat createLiteralFormat(String literal) {
        String escapedLiteral = GeneralUtil.escapeXml(literal);

        return new LiteralFormat(escapedLiteral);
    }
}
