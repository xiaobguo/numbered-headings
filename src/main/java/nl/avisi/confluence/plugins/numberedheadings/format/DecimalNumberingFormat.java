package nl.avisi.confluence.plugins.numberedheadings.format;

public class DecimalNumberingFormat extends AbstractFormat {

    @Override
    public String format(int[] breadcrumbs, Integer level) {
        return Integer.toString(breadcrumbs[level - 1]);
    }

    @Override
    public boolean supportsZeroBasedNumbering() {
        return true;
    }
}
