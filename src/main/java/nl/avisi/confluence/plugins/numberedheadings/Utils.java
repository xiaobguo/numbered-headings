package nl.avisi.confluence.plugins.numberedheadings;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Utils {

    private Utils() {
        // Utility class, so no instantiation
    }

    public static String[] splitAndPreserveToken(String text, String regex) {
        int lastMatch = 0;
        List<String> splitted = new ArrayList<String>();

        Matcher matcher = Pattern.compile(regex).matcher(text);

        while (matcher.find()) {
            String subString = text.substring(lastMatch, matcher.start());
            if (subString.length() > 0) {
                splitted.add(subString);
            }
            splitted.add(matcher.group());

            lastMatch = matcher.end();
        }

        String lastString = text.substring(lastMatch);

        if (!StringUtils.isEmpty(lastString)) {
            splitted.add(text.substring(lastMatch));
        }

        return splitted.toArray(new String[splitted.size()]);
    }

    public static String renderErrorBlock(String title, String message) {
        StringBuilder errorBlock = new StringBuilder();
        errorBlock.append("<div class=\"aui-message warning\">");
        errorBlock.append("  <p class=\"title\">");
        errorBlock.append("    <span class=\"aui-icon icon-warning\"></span>");
        errorBlock.append("    <strong>").append(title).append("</strong>");
        errorBlock.append("  </p>");
        errorBlock.append("  <p>").append(message).append("</p>");
        errorBlock.append("</div>");

        return errorBlock.toString();
    }
}
