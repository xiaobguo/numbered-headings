package nl.avisi.confluence.plugins.numberedheadings.rendering;

public class RenderResult {

    private final String result;
    private final int[] endingNumbers;

    public RenderResult(String result, int[] endingNumbers) {
        this.result = result;
        this.endingNumbers = endingNumbers;
    }

    public String getResult() {
        return result;
    }

    public int[] getEndingNumbers() {
        return endingNumbers;
    }
}
