package nl.avisi.confluence.plugins.numberedheadings;

import com.atlassian.plugin.PluginAccessor;

import nl.avisi.confluence.plugins.numberedheadings.format.DecimalNumberingFormat;
import nl.avisi.confluence.plugins.numberedheadings.format.NumberingFormat;

import java.util.ArrayList;
import java.util.List;

public class NumberFormatterManager {

    private final PluginAccessor pluginAccessor;

    public NumberFormatterManager(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public NumberingFormat fromString(String numberFormatString) {
        // Do not cache this, modules can be enabled/disabled at any time...
        List<NumberFormatterDescriptor> numberFormatterDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(NumberFormatterDescriptor.class);

        for (NumberFormatterDescriptor numberFormatterDescriptor : numberFormatterDescriptors) {
            if (numberFormatterDescriptor.getName().equals(numberFormatString)) {
                return numberFormatterDescriptor.getModule();
            }
        }

        return new DecimalNumberingFormat();
    }

    public List<String> getNumberFormatterDescriptorNames() {
        List<NumberFormatterDescriptor> numberFormatterDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(NumberFormatterDescriptor.class);
        List<String> numberFormatterDescriptorNames = new ArrayList<String>(numberFormatterDescriptors.size());

        for (NumberFormatterDescriptor numberFormatterDescriptor : numberFormatterDescriptors) {
            numberFormatterDescriptorNames.add(numberFormatterDescriptor.getName());
        }

        return numberFormatterDescriptorNames;
    }
}
