package nl.avisi.confluence.plugins.numberedheadings.format;

public class LowerRomanNumberingFormat extends UpperRomanNumberingFormat {

    @Override
    public String format(int[] breadCrumbs, Integer level) {
        return super.format(breadCrumbs, level).toLowerCase();
    }
}
