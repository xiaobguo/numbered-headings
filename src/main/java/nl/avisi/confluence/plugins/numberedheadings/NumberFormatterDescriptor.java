package nl.avisi.confluence.plugins.numberedheadings;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;

import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsException;
import nl.avisi.confluence.plugins.numberedheadings.format.NumberingFormat;

public class NumberFormatterDescriptor extends AbstractModuleDescriptor<NumberingFormat> {

    @Override
    public NumberingFormat getModule() {
        try {
            // According to the documentation the following snippet should have been used, but it did not work...
            // return moduleFactory.createModule(moduleClassName, this);

            return getModuleClass().newInstance();
        } catch (InstantiationException e) {
            throw new NumberedHeadingsException("Could not instantiate numbering formatter", e);
        } catch (IllegalAccessException e) {
            throw new NumberedHeadingsException("Could not access numbering formatter", e);
        }
    }
}
