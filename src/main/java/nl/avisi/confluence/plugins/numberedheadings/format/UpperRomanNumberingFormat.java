package nl.avisi.confluence.plugins.numberedheadings.format;

import nl.avisi.confluence.plugins.numberedheadings.exception.NumberedHeadingsMacroException;

public class UpperRomanNumberingFormat extends AbstractFormat {

    private static final String[] UPPER_ROMAN_UNITS = // Units 0 to 9
            {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};

    private static final String[] UPPER_ROMAN_TENS = // Tens 0 to 90
            {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};

    private static final String[] UPPER_ROMAN_HUNDREDS = // Hundreds 0 to 900
            {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};

    private static final int MAX_NUMBERING = 999;

    @Override
    public String format(int[] breadCrumbs, Integer level) {
        Integer numberToFormat = breadCrumbs[level - 1];

        if (numberToFormat > MAX_NUMBERING) {
            throw new NumberedHeadingsMacroException("The roman formatter does not support numbering above " + MAX_NUMBERING);
        }

        return UPPER_ROMAN_HUNDREDS[(numberToFormat / 100) % 10] + UPPER_ROMAN_TENS[(numberToFormat / 10) % 10] + UPPER_ROMAN_UNITS[(numberToFormat) % 10];
    }
}
